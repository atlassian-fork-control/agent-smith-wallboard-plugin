package com.atlassian.bamboo.plugin.agentsmith;

import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;

/**
 * Service providing the current statistics on Bamboo.
 */
public interface AgentSmithService {
    /**
     * Gets the current statistics.
     *
     * @return the statistics.
     */
    Statistics getStatistics();
}
